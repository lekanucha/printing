

	
			<h4 class="c2"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Printer A Control</h4>
			<table class="table table-condensed c2">
				<tr>
					<td class="td_label">Directory</td>
					<td id="td_path_p1"></td>
				</tr>
				<tr>
					<td class="td_label">Files</td>
					<td>
						<select class="form-control input-sm" id="files_p1" size="15" onchange="set_file(this);">
	
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label txt_label">บรรทัด</td>
					<td class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control input-sm" id="line_p1" placeholder="0" value="">
						</div>
						<button type="button" class="btn btn-default btn-sm"
							onclick="btn_send('line:'+line_p1.value)">SET</button>
					</td>
				</tr>
				<tr>
					<td class="td_label txt_label">โค้ด</td>
					<td class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control input-sm" id="code_p1" placeholder="0" value="">
						</div>
						<button type="button" class="btn btn-default btn-sm"
							onclick="btn_send('code:'+code_p1.value)">SET</button>
					</td>
				</tr>
				<tr>
					<td class="td_label txt_label">SW Speed</td>
					<td class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control input-sm" id="speed_p1" placeholder="0" value="">
						</div>
						<button type="button" class="btn btn-default btn-sm" 
							onclick="btn_send('sw speed:'+speed_p1.value)">SET</button>
					</td>
				</tr>
				<tr>
					<td class="td_label form-inline" colspan="2"><!-- </td>
					<td class="form-inline"> -->
						<div class="checkbox">
							<label>
								<input type="checkbox" id="start001_p1"
									onclick="btn_send('start 001:'+checked)"> ใช้ START 001
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" id="blank_p1"
									onclick="btn_send('blank:'+checked)"> ยิง Blank
							</label>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="bottom">
						<div class="row row-eq-height">
							<div class="col-xs-8 status">
								<!-- <div class="row"> -->
									<!-- <div class="col-xs-6 status"> -->
								<div>
									<div>
								No. of print
									<strong id="no1_p1">000</strong></br>
									<span id="no2_p1">000000000000000000000</span>
								</div>
								<div>
									<hr>
									<span>
								ระดับหมึกพิมพ์ 1 : <strong id="ink1_p1">0.0 ml</strong>
							</span>
							<span>
								ระดับหมึกพิมพ์ 2 : <strong id="ink2_p1">0.0 ml</strong>
							</span>
							</div>

									</div>
							<!-- </div> -->
							<!-- <div class="col-xs-6 status"> -->
								
							<!-- </div> -->
							<!-- </div> -->
							</div>
							<div class="col-xs-4">
								<button type="button" class="btn btn-primary btn-block" 
									onclick="btn_send('pause:')">PAUSE</button>
								<button type="button" class="btn btn-primary btn-block" 
									onclick="btn_send('resume:')">RESUME</button>
								<button type="button" class="btn btn-primary btn-block" 
									onclick="btn_send('start:')">START <span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
							</div>
						</div>
					</td>
				</tr>
			</table>
			


