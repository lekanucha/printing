

	
			<h4 class="c2"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Printer A Control</h4>
			<table class="table table-condensed c2">
				<tr>
					<td class="td_label">Directory</td>
					<td id="td_path_p2"></td>
				</tr>
				<tr>
					<td class="td_label">Files</td>
					<td>
						<select class="form-control input-sm" id="files_p2" size="15">
	
						</select>
					</td>
				</tr>
				<tr>
					<td class="td_label txt_label">บรรทัด</td>
					<td class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control input-sm" id="line_p2" placeholder="0" value="">
						</div>
						<button type="button" class="btn btn-default btn-sm"
							onclick="btn_send2('line:'+line_p2.value)">SET</button>
					</td>
				</tr>
				<tr>
					<td class="td_label txt_label">โค้ด</td>
					<td class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control input-sm" id="code_p2" placeholder="0" value="">
						</div>
						<button type="button" class="btn btn-default btn-sm"
							onclick="btn_send2('code:'+code_p2.value)">SET</button>
					</td>
				</tr>
				<tr>
					<td class="td_label txt_label">SW Speed</td>
					<td class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control input-sm" id="speed_p2" placeholder="0" value="">
						</div>
						<button type="button" class="btn btn-default btn-sm" 
							onclick="btn_send2('sw speed:'+speed_p2.value)">SET</button>
					</td>
				</tr>
				<tr>
					<td class="td_label form-inline" colspan="2"><!-- </td>
					<td class="form-inline"> -->
						<div class="checkbox">
							<label>
								<input type="checkbox" id="start001_p2"
									onclick="btn_send2('start 001:'+checked)"> ใช้ START 001
							</label>
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" id="blank_p2"
									onclick="btn_send2('blank:'+checked)"> ยิง Blank
							</label>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="bottom">
						<div class="row row-eq-height">
							<div class="col-xs-8 status">
								<!-- <div class="row"> -->
									<!-- <div class="col-xs-6 status"> -->
								<div>
									<div>
								No. of print
									<strong id="no1_p2">000</strong></br>
									<span id="no2_p2">000000000000000000000</span>
								</div>
								<div>
									<hr>
									<span>
								ระดับหมึกพิมพ์ 1 : <strong id="ink1_p2">0.0 ml</strong>
							</span>
							<span>
								ระดับหมึกพิมพ์ 2 : <strong id="ink2_p2">0.0 ml</strong>
							</span>
							</div>

									</div>
							<!-- </div> -->
							<!-- <div class="col-xs-6 status"> -->
								
							<!-- </div> -->
							<!-- </div> -->
							</div>
							<div class="col-xs-4">
								<button type="button" class="btn btn-primary btn-block" 
									onclick="btn_send2('pause:')">PAUSE</button>
								<button type="button" class="btn btn-primary btn-block" 
									onclick="btn_send2('resume:')">RESUME</button>
								<button type="button" class="btn btn-primary btn-block" 
									onclick="openSocket2('start:')">START <span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
							</div>
						</div>
					</td>
				</tr>
			</table>
			


