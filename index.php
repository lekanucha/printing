<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Print App</title>
	<link href="stylesheets/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<div class="container raspberyscreen">
		<div class="row">
			<div class="col-xs-12">
				<?php 
					include "printer.php";
				?>
			</div>
		</div>
	</div>
	<!-- /.container -->



	<script src="javascripts/jquery-2.2.4.min.js"></script>
	<script src="javascripts/bootstrap.min.js"></script>
	<script src="javascripts/printer.js"></script>

</body>