var start_url = "http://localhost:7777/?port=";
var wait_startup = 10000;

var ws = new_ws(1, true);



function new_ws(N, first_connect)
{
//var first_connect = true;

var ws_port = 8080+(N-1);
var ws_url = "ws://localhost:"+ ws_port;

console.log('opening:'+ws_url+' , first_connect = '+first_connect);

ws = new WebSocket(ws_url);

ws.onopen = function(event){
    ws.send("open:");
}

ws.onerror = function(){

    console.log("onerror");

	if(first_connect)
      {
		 first_connected = false; 
	     console.log("try starting app on port "+ ws_port +" and reconnect...");	  
		 
		 var _url = start_url+ws_port;
		 $.ajax(_url);

		 setTimeout(function(){ //alert('reconnect')
            ws = new_ws(N, false);
		 }, wait_startup);
	  }
	else
	  {
	     console.log("first_connect = "+first_connect);
	  }
}

ws.onmessage = function(event){

    var data = event.data.split(":");

	if(data[0] == "path") $("#td_path_p"+N).text(data[1]);
    if(data[0] == "files"){
        $.each(event.data.split(":"), function(key, value) { 
            if(key>0)
                $('#files_p'+N).append($("<option></option>").text(value)); 
        });
    }
    if(data[0] == "line") $("#line_p"+N).val(data[1]);
    if(data[0] == "code") $("#code_p"+N).val(data[1]);
    if(data[0] == "sw speed") $("#speed_p"+N).val(data[1]);
    if(data[0] == "no1") $("#no1_p"+N).text(data[1]);
    if(data[0] == "no2") $("#no2_p+"+N).text(data[1]);
    if(data[0] == "ink1") $("#ink1_p"+N).text(data[1]);
    if(data[0] == "ink2") $("#ink2_p"+N).text(data[1]);
};
/*
function openSocket(val){

    var line = $("#line_p"+N).val();
    var code = $("#code_p"+N).val();
    var sw_speed = $("#speed_p"+N).val();
    var start001 = $("#start001_p"+N).prop('checked');
    var blank = $("#blank_p"+N).prop('checked');

    if(ws !== undefined && ws.readyState !== WebSocket.CLOSED){
        console.log("WebSocket is already opened.");
        ws.send(val+line+':'+code+':'+sw_speed+':'+start001+':'+blank);
        return;
    }
    ws = new WebSocket(ws_url);      
    ws.onopen = function(event){
        ws.send(val+line+':'+code+':'+sw_speed+':'+start001+':'+blank);
    }

}*/ 


ws.onclose = function(){ 
    console.log("Connection is closed");
};

return ws;
}// new_ws()


 


//------ entry point
 function set_file(ele)
 {
   var file = ele[ele.selectedIndex].value;
   ws.send('setf:'+file);
 }

function btn_send(val){
    console.log('btn_send (1) = '+val);
    ws.send(val);
}
